from django.contrib.auth import get_user_model
from django.test import TestCase
from django.urls import reverse

from .models import Characters


# Create your tests here.

class CharacterTests(TestCase):

    def setup(self):

        self.user = get_user_model().objects.create_user(
            username='testuser',
            email='test@email.com',
            password='secret'
        )

        self.character = Characters.objects.create(
             name='Wario',
             description='Marios gold crazed cousin',
             gameSeries='1',

        )

    def test_string_representation(self):
        character = Characters(name='Wario')
        self.assertEqual(str(character), character.name)

    def test_character_content(self):
        self.assertEqual(f'{self.character.name}', 'Wario')
        self.assertEqual(f'{self.character.description}', 'Marios gold crazed cousin')
        self.assertEqual(f'{self.character.gameSeries}', '1')

    def test_character_list_view(self):
        response = self.client.get(reverse('home'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Marios gold crazed cousin')
        self.assertTemplateUsed(response, 'home.html')

    def test_character_detail_view(self):
        response = self.client.get('/post/1/')
        no_response = self.client.get('/post/100000/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(no_response.status_code, 404)
        self.assertContains(response, 'Wario')
        self.assertContains(response, 'character_detail.html')





